from sklearn.metrics import roc_auc_score, accuracy_score, \
    balanced_accuracy_score, f1_score, recall_score, precision_score
from sklearn.model_selection import GridSearchCV
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import roc_curve, confusion_matrix, auc, \
    precision_recall_curve, average_precision_score
from lightgbm import LGBMClassifier
import sys
from pathlib import Path
import pandas as pd
import os
import numpy as np
import pickle


###########################################################################################################
def getData(partition, X, cv):
    tiles_trn = list(set(partition['train_' + str(cv)]) & set(data.index))
    tiles_test = list(set(partition['val_' + str(cv)]) & set(data.index))
    X_train = X.loc[tiles_trn, :]
    # X_val = X.loc[partition['validation' + str(cv)], :]
    X_test = X.loc[tiles_test, :]

    return X_train, X_test


def getLabels(partition, labels, cv):
    tiles_trn = list(set(partition['train_' + str(cv)]) & set(data.index))
    tiles_test = list(set(partition['val_' + str(cv)]) & set(data.index))

    # Generate data
    y_trn = y.loc[tiles_trn]  # [labels[ID] for i, ID in enumerate(partition['train' + str(cv)][0])]
    y_tst = y.loc[tiles_test]  # [labels[ID] for i, ID in enumerate(partition['val_' + str(cv)][0])]
    return y_trn, y_tst


def perf_90recall(models, X_tests, y_tests):
    if len(np.unique(y_tests)) == 2:

        pred_test = []
        y_test_merged = y_tests  # list(chain(*y_tests))

        pred_test = models.predict_proba(X_tests)[:, 1]

        precisions, recalls, thresholds = precision_recall_curve(y_test_merged, pred_test)
        threshold = thresholds[np.abs(recalls - 0.9).argmin()]

        pred_test = (models.predict_proba(X_tests)[:, 1] >= threshold).astype(int)

        tn, fp, fn, tp = confusion_matrix(y_test_merged, pred_test).ravel()
        performance = {}
        performance['precision'] = tp / (tp + fp)
        performance['specificity'] = tn / (tn + fp)
        performance['sensitivity'] = tp / (tp + fn)
        performance['f1'] = f1_score(y_test_merged, pred_test)
        performance['npv'] = tn / (tn + fn)
        performance['accuracy'] = (tp + tn) / (tp + fp + tn + fn)
        performance['recall'] = tp / (tp + fn)

        y_score = models.predict_proba(X_tests)[:, 1]
        aps = average_precision_score(y_test_merged, y_score)
        fp_rates, tp_rates, _ = roc_curve(y_test_merged, y_score)
        roc_auc = auc(fp_rates, tp_rates)


    return tn, fp, fn, tp, performance['accuracy'], performance['precision'], \
        performance['recall'], roc_auc, aps, performance['f1'],y_score


def getPredictionSingleFold_nooutput(cv, partition, labels, clf_choice, clinical):
    # Get the labels
    y_train, y_test = getLabels(partition, labels, cv)
    y_trainVal = y_train  # + y_val

    # Get data
    X_trainVal, X_test = getData(partition, clinical, cv)

    # Prediction
    if clf_choice == 'lightGBM':
        # Initialise model, optimise hyperparameters by random sampling
        clf = LGBMClassifier(class_weight='balanced', random_state=1)

        # Maximum tree leaves for base learners
        num_leaves = [5, 15, 31]

        # Boosting learning rate
        learning_rate = [0.1, 0.01]

        # Number of boosted trees to fit
        n_estimators = [200, 1000]

        # L1 regularisation term on weights
        reg_alpha = [0, 1e-02]

        # L2 regularisation term on weights
        reg_lambda = [0, 1e-02]

        # Create the random grid
        param_grid = {'num_leaves': num_leaves,
                      'learning_rate': learning_rate,
                      'n_estimators': n_estimators,
                      'reg_alpha': reg_alpha,
                      'reg_lambda': reg_lambda}

        # Could replace for random search here, too
        clf = GridSearchCV(clf, param_grid, cv=3, verbose=1, scoring='roc_auc')

    # Fit
    clf.fit(X_trainVal, y_trainVal)

    # Save best estimator
    best_est = clf.best_estimator_
    best_est.fit(X_trainVal, y_trainVal)

    return best_est, X_trainVal, X_test


###########################################################################################################
###########################################################################################################

def makeprediction(celltypes,i_cell,path_data,suffix,cv):
    celltype   = celltypes[i_cell]
    clf_choice = 'lightGBM'


    # data and saving paths
    path_labels  = os.path.join(path_data,'preparedData')
    output_model = os.path.join(path_data,'models'+suffix)
    Path(output_model).mkdir(parents=True, exist_ok=True)
    name_save    = 'tilesVHE_' +celltype+'_'+clf_choice+'_'+str(cv)

    # load data
    data         = pd.read_csv(os.path.join(path_labels, 'tiles', 'features'+suffix+'.csv'), index_col=0)
    all_labels   = pd.read_csv(os.path.join(path_labels, 'tiles', 'labels'+suffix+'.csv'), index_col=0)
    data_TEST         = pd.read_csv(os.path.join(path_labels, 'tiles', 'features_TEST'+suffix+'.csv'), index_col=0)
    all_labels_TEST   = pd.read_csv(os.path.join(path_labels, 'tiles', 'labels_TEST'+suffix+'.csv'), index_col=0)


    # Get label
    y = (all_labels[celltype]>0).astype('int')
    y_TEST = (all_labels_TEST[celltype]>0).astype('int')



    # Tile based prediction - get splits
    with open(os.path.join(path_labels, 'partition_tile'+suffix), 'rb') as file:
        partition = pickle.load(file)


    newind = [i.split('.png')[0] for i in data.index]
    data['ind'] = newind
    data.set_index('ind', inplace = True)

    newind_TEST = [i.split('.png')[0] for i in data_TEST.index]
    data_TEST['ind'] = newind_TEST
    data_TEST.set_index('ind', inplace = True)
    y_TEST = y_TEST.loc[data_TEST.index]


    # Scale data
    tiles_trn = list(set(partition['train_' + str(cv)])&set(data.index))
    tiles_test = list(set(partition['val_' + str(cv)])&set(data.index))
    sc = StandardScaler()
    npx_reform_train = pd.DataFrame(sc.fit_transform(
        data.loc[tiles_trn, :].values.astype(float)),
        index=data.loc[tiles_trn, :].index,
        columns=data.columns)

    npx_reform_val = pd.DataFrame(sc.transform(
        data.loc[tiles_test, :].values),
        index=data.loc[tiles_test, :].index,
        columns=data.columns)
    y_test = y.loc[tiles_test]

    npx_TEST = pd.DataFrame(sc.transform(
        data_TEST.loc[:, :].values),
        index=data_TEST.loc[:, :].index,
        columns=data_TEST.columns)

    data_norm = pd.concat([npx_reform_train,npx_reform_val],axis = 0)


    #################################################################################################################
    # Predict with all features
    print('Prediction with all features')
    best_est_cv_all, X_train_all, X_test_all = \
        getPredictionSingleFold_nooutput(cv, partition, y, clf_choice, data_norm)
    pickle.dump(best_est_cv_all, open(os.path.join(output_model, name_save + '_model.save'), 'wb'))

    # Evaluate performance on internal test set (all features)
    tn, fp, fn, tp, accuracy, precision, recall, \
        roc_auc_top, aps_top, f1, y_pred = perf_90recall(best_est_cv_all, X_test_all, y_test)
    prev = np.mean(y.loc[X_test_all.index])
    df_performance = pd.DataFrame(columns = ['prev', 'roc_auc_top', 'aps_top', 'aps_top / prev',
                                             'tn', 'fp', 'fn', 'tp', 'accuracy',
                                             'precision', 'recall'])
    df_performance.loc['all_features', :] = [prev, roc_auc_top, aps_top, aps_top / prev, tn, fp, fn, tp, accuracy,
                                             precision, recall]
    df_performance.to_csv(os.path.join(output_model,name_save+'_performance.csv'))
    df_pred_label = pd.DataFrame(y_pred, index = tiles_test)
    df_pred_label.to_csv(os.path.join(output_model,name_save+'_predictionProbabilities.csv'))




    # Evaluate performance (all features) for TEST (external test set)
    tn_TEST, fp_TEST, fn_TEST, tp_TEST, accuracy_TEST, precision_TEST, recall_TEST, \
        roc_auc_top_TEST, aps_top_TEST, f1_TEST, y_pred_TEST = perf_90recall(best_est_cv_all, npx_TEST, y_TEST)
    prev_TEST = np.mean(y_TEST.loc[ npx_TEST.index])
    df_performance_TEST = pd.DataFrame(columns = ['prev', 'roc_auc_top', 'aps_top', 'aps_top / prev',
                                             'tn', 'fp', 'fn', 'tp', 'accuracy',
                                             'precision', 'recall'])
    df_performance_TEST.loc['all_features', :] = [prev_TEST, roc_auc_top_TEST, aps_top_TEST, aps_top_TEST / prev_TEST, tn_TEST, fp_TEST, fn_TEST, tp_TEST, accuracy_TEST,precision_TEST, recall_TEST]
    df_performance_TEST.to_csv(os.path.join(output_model,name_save+'_performance_TEST.csv'))



    df_pred_label_TEST = pd.DataFrame(y_pred_TEST, index =y_TEST.index )
    df_pred_label_TEST.to_csv(os.path.join(output_model,name_save+'_predictionProbabilities_TEST.csv'))


