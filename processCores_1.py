import pandas as pd
import os
from pathlib import Path
from tqdm import tqdm
from utilities import dict_stainloc

####################################################################
def getAllMarkers(df_cells_train):
    # Get markers - 33 including 2 Dapi and '14-3-3-SIGMA'
    cats = ['Nucleus', 'Cell', 'Cytoplasm']
    suffix = ['std dev', 'max', 'min', 'mean', 'range', 'sum']
    markers = []
    for c in df_cells_train.columns[14:-1]:
        for cat in cats:
            for s in suffix:
                if (s in c) & (cat in c):
                    m = c.split(cat + ': ')[1].split(' ' + s)[0]
                    if m not in markers:
                        markers.append(m)
    return markers

####################################################################
def processCores_1(path_data, suffix,filepath_train, filepath_test,thresholdfile_train,thresholdfile_test, metadatafile,resolution ):

    for useTest in [True, False]:

        # Names
        if useTest:
            id = 273
            filepath = filepath_train
            thresholdfile = thresholdfile_train
        else:
            id = 272
            filepath = filepath_test
            thresholdfile = thresholdfile_test


        # Create output
        Path(os.path.join(path_data, 'cores_cellData')).mkdir(parents=True, exist_ok=True)
        Path(os.path.join(path_data, 'preparedData', 'cores' + suffix)).mkdir(parents=True, exist_ok=True)

        # Load data
        df_cells  = pd.read_csv(filepath, index_col=0)
        df_thresh = pd.read_csv(thresholdfile, index_col=0)
        markers_prelim = list(set(df_thresh.columns.to_list()) & set(dict_stainloc.keys()))

        # Get Markers
        cols_cells = df_cells.columns
        markers = []
        cols_marker = []
        for m in markers_prelim:

            if m == 'KI-67':
                c = dict_stainloc[m] + ': ' + 'KI67' + ' mean'
            elif m == 'VGFR2':
                c = dict_stainloc[m] + ': ' + 'VEGFR2' + ' mean'
            elif m == 'PDGFR':
                c = dict_stainloc[m] + ': ' + 'PDGFRA' + ' mean'
            elif m == 'Vimentin':
                c = dict_stainloc[m] + ': ' + 'VIMENTIN' + ' mean'
            elif m == 'Stat3':
                c = dict_stainloc[m] + ': ' + 'STAT3' + ' mean'
            elif m == 'Nestin':
                c = dict_stainloc[m] + ': ' + 'NESTIN' + ' mean'
            elif m == 'Iba1':
                c = dict_stainloc[m] + ': ' + 'IBA1' + ' mean'
            else:
                c = dict_stainloc[m] + ': ' + m + ' mean'

            if c not in cols_cells:
                print('Missing: ' + c)
            else:
                markers.append(m)
                cols_marker.append(c)

            if m == 'Iba1':
                c2 = 'Cell' + ': ' + 'IBA1' + ' mean'
                if c2 not in cols_cells:
                    print('Missing: ' + c2)
                else:
                    if m not in markers:
                        markers.append(m)
                    cols_marker.append(c2)

        cores       = df_thresh.index.to_list()
        df_meta_all = pd.read_csv(metadatafile)
        df_meta_all = df_meta_all[df_meta_all.TMA == id]
        df_meta_all.set_index('Name', inplace=True)

        # Loop over cores
        df_meta = pd.DataFrame()
        for core in tqdm(cores):

            name = core + '_' + str(id)
            # print(name)

            # Get core subset
            df_core = df_cells[df_cells['TMA core'] == core][cols_marker + ['Centroid X Âµm', 'Centroid Y Âµm']]

            n_cells = df_core.shape[0]
            if n_cells < 10:
                continue

            # Get core meta data
            df_core['x_loc'] = (df_core['Centroid X Âµm'] / resolution).astype(int)
            df_core['y_loc'] = (df_core['Centroid Y Âµm'] / resolution).astype(int)
            df_core.to_csv(os.path.join(path_data, 'preparedData', 'cores' + suffix, 'cell_data_' + name + '.csv'))

            min_x_core = df_core['x_loc'].min()
            min_y_core = df_core['y_loc'].min()
            max_x_core = df_core['x_loc'].max()
            max_y_core = df_core['y_loc'].max()

            df_meta.loc[name, 'min_x_core'] = min_x_core
            df_meta.loc[name, 'min_y_core'] = min_y_core
            df_meta.loc[name, 'max_x_core'] = max_x_core
            df_meta.loc[name, 'max_y_core'] = max_y_core

            # Get markers and cell numbers
            df_meta.loc[name, 'n_cells'] = n_cells
            for m in markers:

                try:
                    thresh = float(df_thresh.loc[core, m])
                except:
                    print('failed ' + m + ' for core ' + core)
                    continue

                if m == 'KI-67':
                    col = dict_stainloc[m] + ': ' + 'KI67' + ' mean'
                elif m == 'VGFR2':
                    col = dict_stainloc[m] + ': ' + 'VEGFR2' + ' mean'
                elif m == 'PDGFR':
                    col = dict_stainloc[m] + ': ' + 'PDGFRA' + ' mean'
                elif m == 'Vimentin':
                    col = dict_stainloc[m] + ': ' + 'VIMENTIN' + ' mean'
                elif m == 'Stat3':
                    col = dict_stainloc[m] + ': ' + 'STAT3' + ' mean'
                elif m == 'Nestin':
                    col = dict_stainloc[m] + ': ' + 'NESTIN' + ' mean'
                else:
                    if m == 'Iba1':
                        col = dict_stainloc[m] + ': ' + 'IBA1' + ' mean'
                        if thresh == 2116:
                            # print('Iba Exception!!!')
                            col = 'Cell' + ': ' + 'IBA1' + ' mean'
                        else:
                            col = dict_stainloc[m] + ': ' + 'IBA1' + ' mean'
                    else:
                        col = dict_stainloc[m] + ': ' + m + ' mean'

                n_cellsM = (df_core[col] > thresh).sum()
                df_meta.loc[name, m + '_posCells'] = n_cellsM

            # Some more meta data
            df_meta.loc[name, 'is_healthy'] = df_meta_all.loc[core, 'Diagnosis'] == 'CNTRL'
            df_meta.loc[name, 'is_DMG'] = df_meta_all.loc[core, 'Diagnosis'] in ['DIPG', 'DMG spinal', 'DMG Thalamic',
                                                                                 'DMG Thalamic ']
            df_meta.loc[name, df_meta_all.columns] = df_meta_all.loc[core]

        # Save
        df_meta.to_csv(os.path.join(path_data, 'preparedData', 'metadataCores_' + str(id) + suffix + '.csv'))
