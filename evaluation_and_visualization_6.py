import pandas as pd
import numpy as np
import os
import matplotlib.pyplot as plt
from pathlib import Path


def plotBars(grouped_df, folder, prefix, col='roc_auc_top', col_name='ROCAUC', randomthresh=0.5, useYlim=True):

    Path(output).mkdir(parents=True, exist_ok=True)

    # sorting
    grouped_df.sort_values((col, 'mean'), ascending=False, inplace=True)

    # Plotting the bar plot with error bars
    plt.figure(figsize=(5, 3))

    # Getting the x values from the index (assuming 'celltype' is the index)
    x_values = np.arange(len(grouped_df.index))  # grouped_df.index

    # Getting the y values (means) and standard deviations
    y_values = grouped_df[col]['mean']
    std_values = grouped_df[col]['std']

    # Plotting the bars with error bars
    plt.bar(x_values, y_values, yerr=std_values, capsize=5, color='lightgrey')

    # Adding labels and title
    # plt.xlabel('Marker', fontsize = 12)
    plt.ylabel(col_name, fontsize=12)

    # Customizing x-axis labels
    plt.xticks(x_values, grouped_df['celltype'], rotation=90)

    # Custom y range
    if useYlim:
        plt.ylim([np.min([0.45, np.min(y_values)]), np.max([1.05, np.max(y_values)])])

    # Random
    plt.hlines(randomthresh, -1, len(grouped_df.index), colors='r', linestyles='--', label='random classifier')

    # Showing the plot
    plt.legend()
    plt.tight_layout()
    plt.savefig(os.path.join(folder, prefix + 'performance_barplot_' + col_name + '.png'))


def evalPerformance(folder, prefix, celltypes, output):
    df_Perf = pd.DataFrame()
    counter = 0
    for celltype in celltypes:
        for m in ['lightGBM']:  # 'RF.'LR','MLP',
            for cv in range(0, 5):
                name = m + '_' + str(cv)

                if prefix == 'Train':
                    filename = 'tilesVHE_' + celltype + '_' + name + '_performance.csv'
                else:
                    filename = 'tilesVHE_' + celltype + '_' + name + '_performance' + prefix + '.csv'

                try:
                    df_cv = pd.read_csv(os.path.join(folder, filename), index_col=0)
                except:
                    print('Missing fold ' + str(cv) + ' for ' + celltype)
                df_cv['ind'] = [name]
                df_cv.set_index('ind', inplace=True)
                df_cv['model'] = m
                df_cv['cv'] = cv
                df_cv['celltype'] = celltype
                if counter == 0:
                    df_Perf = df_cv
                else:
                    df_Perf = pd.concat([df_Perf, df_cv], axis=0)
                counter += 1

        # plot

    # Summarize
    df_Perf.groupby(['celltype']).agg(['mean', 'std'])
    df_Perf.to_csv(os.path.join(folder, prefix + 'performanceSummary.csv'))
    grouped_df = df_Perf.groupby(['celltype']).agg(['mean', 'std'])
    grouped_df.sort_values(('roc_auc_top', 'mean'), ascending=False, inplace=True)

    # Resetting the index for ease of use
    grouped_df.reset_index(inplace=True)
    plotBars(grouped_df, output, prefix, col='roc_auc_top', col_name='ROCAUC', randomthresh=0.5)
    plotBars(grouped_df, output, prefix, col='aps_top / prev', col_name='rel.AUPRC', randomthresh=1, useYlim=False)

    return grouped_df


#################################################################################
