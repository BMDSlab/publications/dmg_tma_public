# DMG_TMA_public
This repository summarizes all processing steps in order to obtain predictions for a set of 22 protein markers within virtual H\&E slides presented as part of our publication (link to follow). 
This includes data preparation, splitting, model training and evaluation. 

This pipeline builds on the implementation of RetCCL (from https://github.com/Xiyue-Wang/RetCCL, accessed in December 2023) based on their publication: https://www.sciencedirect.com/science/article/abs/pii/S1361841522002730 


## Getting started
Please follow the instructions on https://github.com/Xiyue-Wang/RetCCL regarding the installation of requirements for the image feature extraction pipeline. 
Additionally, only the following packages need to be installed - relevant version numbers are given in brackets.

scikit-learn (1.3.2)<br>
matplotlib (3.8.2)<br>
lightgbm (4.3.0)

## Usage 
We summarize all code within _main.py_ ready for execution upon data request. This repository does not comprise the original data required which has to be requested separately.

### 1) Metadata preparation
Exploration of the data based on the summary referring to the origin, diagnosis and annotation of the different TMA cores. 

### 2) Image patching
Patch the large virtual H\&E file into quadratic tiles of a predefined edge length - these are saved independently. For each tile, the information regarding the cells represented within it are summarized. 

### 3) Data split
Split the data of TMA 1 (272) into training, and internal test set, stratified for the label of DMG. Note that internally, a further nested split will be performed upon model training for hyperparameter tuning. The relevant splits are saved in a dictionary (partition).


### 4) Feature and label extraction
####   4a) Label definition
We summarize the labels for all tiles in a separate data frame.

####   4b) Feature extraction
Features are extracted from rgb image patches using RetCCL. Relevant documentation is referenced in the relevant code. 

####   4c) Feature formatting
We format the tabular features in a data frame indexed by tile indentifier in line with the indexing of the label vector.

### 5) Model training
A lightGBM classifier is trained with nested Gridsearch for hyperparameter tuning. A separate model is trained for each cross validation fold and each protein marker. We generate prediction labels for each tile within the internal test set (left out fraction), and the external test set (TMA2, 273).

### 6) Performance evaluation
The prediction performance of each of the 22 markers is summarized as ROCAUC and AUPRC values across the five cross validation folds. A barplot with relevant errorbars is generation. 

### 7) Visualization
We provide code for the visualization of the predicted, true, and differences between true and predicted labels overlaid with the original rgb virtual H\&E image. 


## Support
Please contact the authors for specific questions under sarah.brueningk@hest.ethz.ch. 

## Authors and acknowledgment
This project was supported by the Basel Research Center for Child Health Postdoctoral Excellence Programme under PEP-2021-1008.

## License
TBA.

## Project status
This project is ongoing and likely to further develop. We provide here the latest version of code for public access.