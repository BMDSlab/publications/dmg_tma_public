# Code obtained from https://github.com/Xiyue-Wang/RetCCL in December 2023
# and minimally modified to enable processing of our tiles - we include the relevant code and license here for your reference
# [Journal Link](https://www.sciencedirect.com/science/article/abs/pii/S1361841522002730)

import pandas as pd
import torch, torchvision
import torch.nn as nn
from torchvision import transforms
from PIL import Image
import ResNet as ResNet
from torch.utils.data import Dataset
import os
import numpy as np
import argparse
from pathlib import Path

parser = argparse.ArgumentParser(description="Process a list of indices indicating the core.")
parser.add_argument("--indices", nargs="+", type=int, help="List of indices")
args = parser.parse_args()

mean = (0.485, 0.456, 0.406)
std = (0.229, 0.224, 0.225)
trnsfrms_val = transforms.Compose(
    [
        transforms.Resize(256),
        transforms.ToTensor(),
        transforms.Normalize(mean=mean, std=std)
    ]
)


class roi_dataset(Dataset):
    def __init__(self, img_csv,
                 ):
        super().__init__()
        self.transform = trnsfrms_val

        self.images_lst = img_csv

    def __len__(self):
        return len(self.images_lst)

    def __getitem__(self, idx):
        path = self.images_lst.filename[idx]
        image = Image.open(path).convert('RGB')
        image = self.transform(image)

        return image


def featureExtraction_4b(i, path_data,suffix, checkpointpath):
    folder_png = os.path.join(path_data, 'preparedData', 'tiles', 'png' + suffix)
    output = os.path.join(path_data, 'preparedData', 'tiles', 'features' + suffix)
    Path(output).mkdir(parents=True, exist_ok=True)

    for id in [272, 273]:
        print(id)

        try:
            df_meta = pd.read_csv(os.path.join(path_data, 'preparedData', 'metadataCores_' + str(id) + suffix + '.csv'),
                                  index_col=0)

            # '/cluster/project/bmds_lab/TMA/data/tiles/png_100'
            core_folders = os.listdir((folder_png))
            if '.DS_Store' in core_folders:
                core_folders.remove('.DS_Store')
            cores = df_meta.index.to_list()
            for core in [cores[i]]:
                print(core)

                n_tiles = df_meta.loc[core, 'n_tiles'].astype(int)
                if not np.isnan(n_tiles):

                    folder = folder_png
                    img_lst = ['tile_' + core + '_' + str(j) + '.png' for j in range(0, n_tiles)]

                    img_csv = pd.DataFrame(img_lst, columns=['filename'])
                    img_csv = folder + '/' + img_csv

                    img_csv = pd.DataFrame(img_lst, columns=['filename'])
                    img_csv = folder + '/' + img_csv
                    test_datat = roi_dataset(img_csv)
                    database_loader = torch.utils.data.DataLoader(test_datat, batch_size=1, shuffle=False)

                    model = ResNet.resnet50(num_classes=128, mlp=False, two_branch=False, normlinear=True)
                    pretext_model = torch.load(checkpointpath,
                                               map_location=torch.device('cpu'))
                    model.fc = nn.Identity()
                    model.load_state_dict(pretext_model, strict=True)

                    model.eval()
                    df_core = pd.DataFrame(columns=np.arange(128))
                    with torch.no_grad():
                        for batch in database_loader:
                            features = model(batch)
                            features = features.cpu().numpy()
                            df_core = pd.concat([pd.DataFrame(features), df_core]).reset_index(drop=True)

                        df_core['ind'] = img_lst
                        df_core.set_index('ind', inplace=True)
                    if df_core.shape[0] > 0:
                        df_core.to_csv(os.path.join(output, core + '.csv'))
        except:
            print('failed ' + str(id))
            continue