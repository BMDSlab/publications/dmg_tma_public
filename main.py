######
# full feature extraction, prediction and evaluation pipeline
# author: S.Brueningk, May 2024

# 1) Metadata preparation
# 2) Image patching
# 3) Data split
# 4) Feature and label extraction
#   4a) Label definition
#   4b) Feature extraction
#   4c) Feature formatting
# 5) Model training
# 6) Performance evaluation
# 7) Visualization

import os
from processCores_1 import processCores_1
from preprocessData_2 import preprocessData_2_allruns
from splitData_3 import splitData_3
from labelpreparation_4a import labelpreparation_4
from CCL_featureExtraction_4b import featureExtraction_4b
from featurePreparation_4c import featurePreparation_4c
from classifierTraining_5 import makeprediction
from evaluation_and_visualization_6 import evalPerformance
from visualization_7 import visualization_7

############################################################################################
# Naming conventions and paths
path_data = 'my_path'

filepath_train      = os.path.join(path_data, 'data', 'TMA272_cellbycell_bundles_1_2_3.csv')
filepath_test       = os.path.join(path_data, 'data', 'TMA273_cellbycell_bundles_1_2_3_without_N_Cores.csv')
metadatafile        = os.path.join(path_data, 'data', 'TMA_Metadata_Sample_information.csv')
thresholdfile_train = os.path.join(path_data, 'data', 'Thresholds_by_cores_272.csv')
thresholdfile_test  = os.path.join(path_data, 'data', 'Thresholds_by_cores_273.csv')
slidename_inttest   = 'S21110002_VHE_wholeslide_Train.tif'
slidename_exttest   = 'S21110003_VHE_wholeslide_Test.tif'
core_image_path     = os.path.join(path_data, 'images_cores') # Individual images of the separate cores

# RetCCL checkpoint downloaded from:
checkpointpath = 'my_path_to_best_ckpt.pth'


# Settings for a single run
suffix     = '_myname'
resolution = 0.325  # mum per pixel for the H&E image used
side       = 50  # in mu m
shift_pix  = int(0.5 * side / resolution)
side_pix   = int(side / resolution)
min_cells  = 1  # minimum number of cells required in a patch to be considered

# Markers considered
celltypes = ['CD3', 'SOX2', 'CD31', 'CD163', 'CD34', 'HUD', 'MBP',
       'IBA1', 'CD8', 'PD1', 'NESTIN', 'VDAC', 'VIMENTIN', 'PDL1', 'VEGFR2',
       'GFAP', 'STAT3', 'KI67', 'CD68', 'PDGFRA', 'H3K27M', 'B7H3']


############################################################################################


# 1) Metadata preparation
processCores_1(path_data, suffix,filepath_train, filepath_test,thresholdfile_train,
               thresholdfile_test, metadatafile,resolution )

# 2) Image patching - we would recommend running this part based on a bash script in a parallelized fashion but provide executable code here for refernce
preprocessData_2_allruns(suffix,path_data,filepath_test,slidename_exttest,
                             filepath_train, slidename_inttest,thresholdfile_train,
                             thresholdfile_test,shift_pix,side_pix,min_cells )

# 3) Data split
splitData_3(path_data,suffix)

# 4) Feature and label extraction
# 4a) Label definition
labelpreparation_4(suffix,path_data)

# 4b) Feature extraction -
# ideally this part is run independently, in parallel for each core
# here we provide code for a single core (number 0) as a proof of principl
i_core = 0
featureExtraction_4b(i_core, path_data,suffix, checkpointpath)

# 4c) Feature formatting
featurePreparation_4c(suffix,path_data)

# 5) Model training - example code for a single fold and single marker
i_marker = 0 # loop over all markers (0 to 21).
cv = 0 # loop over all cross validation fold (0 to 4).
makeprediction(celltypes,i_marker,path_data,suffix,cv)


# 6) Performance evaluation for internal test set (indicated as _train - note that this is not the training set
# performance but indeed the internal test set; and the external test set performance)
df_grouped_test = evalPerformance(os.path.join(path_data, 'models' + suffix),'_TEST',
                                  celltypes,os.path.join(path_data, 'plots' + suffix))
df_grouped_internalTest = evalPerformance(os.path.join(path_data, 'models' + suffix), 'Train', celltypes,
                                   os.path.join(path_data, 'plots' + suffix))


# 7) Visualization - plot spatial predictions for a selected marker and core
cellmarker = 'H3K27M' # This is just an example - specify which marker to use.
cores_in   = ['M-22_273'] # This is just an example - specify the core.
visualization_7(path_data,suffix,cellmarker,resolution,side,min_cells,cores_in, core_image_path)






