# Get features
import os
import pandas as pd
import pickle
from tqdm import tqdm

def featurePreparation_4c(suffix,path_data):

    id_test = 273
    path_labels = os.path.join(path_data, 'preparedData')

    with open(os.path.join(path_labels, 'partition_core' + suffix), 'rb') as file:
        partition_core = pickle.load(file)

    cores = partition_core['train_0'][0] + partition_core['val_0'][0]

    counter = 0
    for i, name in tqdm(enumerate(cores)):

        try:
            df_core = pd.read_csv(os.path.join(path_labels, 'tiles', 'features' + suffix, name + '.csv'), index_col=0)
        except:
            print('Failed ' + name)
            continue

        # Format: 'tile_J-24_272_184'
        if counter == 0:
            df_features = df_core
            counter = 1
        else:
            df_features = pd.concat([df_features, df_core], axis=0)
    df_features.to_csv(os.path.join(path_labels, 'tiles', 'features' + suffix + '.csv'))

    df_meta_test = pd.read_csv(os.path.join(path_data, 'preparedData', 'metadataCores_' + str(id_test) + suffix + '.csv'),
                               index_col=0)
    cores_test = df_meta_test.index.to_list()
    counter = 0
    for i, name in tqdm(enumerate(cores_test)):

        try:
            df_core = pd.read_csv(os.path.join(path_labels, 'tiles', 'features' + suffix, name + '.csv'), index_col=0)
        except:
            print('Failed ' + name)
            continue

        # Format: 'tile_J-24_272_184'
        if counter == 0:
            df_features_test = df_core
            counter = 1
        else:
            df_features_test = pd.concat([df_features_test, df_core], axis=0)
    df_features_test.to_csv(os.path.join(path_labels, 'tiles', 'features_TEST' + suffix + '.csv'))
