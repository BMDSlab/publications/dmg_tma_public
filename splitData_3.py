import os
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from sklearn.model_selection import StratifiedGroupKFold
import pickle
from tqdm import tqdm



def getSplit(data, col_y, n_splits):
    # Group the data by patientID
    groups = data['PID']  # data.groupby('patientID').indices.values()

    # Perform the stratified split based on labels and patient groups
    StratifiedGroupKFold(n_splits=n_splits, random_state=None, shuffle=False)
    sgkf = StratifiedGroupKFold(n_splits=n_splits)
    df_splitsum = pd.DataFrame(index=np.arange(n_splits))
    partition_core = {}
    partition_tile = {}
    for i, (train_index, test_index) in enumerate(sgkf.split(data, data[col_y], groups)):
        df_splitsum.loc[i, 'mean_DMG_prev_train'] = data.iloc[train_index]['is_DMG'].mean()
        df_splitsum.loc[i, 'mean_DMG_prev_test'] = data.iloc[test_index]['is_DMG'].mean()
        df_splitsum.loc[i, 'n_pats_train'] = data.iloc[train_index].PID.nunique()
        df_splitsum.loc[i, 'n_pats_test'] = data.iloc[test_index].PID.nunique()
        df_splitsum.loc[i, 'n_tiles_train'] = data.iloc[train_index]['n_tiles'].mean()
        df_splitsum.loc[i, 'n_tiles_test'] = data.iloc[test_index]['n_tiles'].mean()

        partition_core['train_' + str(i)] = [data.iloc[train_index].index.to_list()]
        partition_core['val_' + str(i)] = [data.iloc[test_index].index.to_list()]

        train_tiles = []
        for core in data.iloc[train_index].index:
            # print(core)
            this_tiles = data.loc[core, 'n_tiles'].astype(int)
            train_tiles += ['tile_' + core + '_' + str(j) for j in range(0, this_tiles)]
        test_tiles = []
        for core in data.iloc[test_index].index:
            this_tiles = data.loc[core, 'n_tiles'].astype(int)
            test_tiles += ['tile_' + core + '_' + str(j) for j in range(0, this_tiles)]

        partition_tile['train_' + str(i)] = train_tiles
        partition_tile['val_' + str(i)] = test_tiles

    return partition_tile, partition_core


markers = \
    ['Vimentin',
     'CD31',
     'Nestin',
     'CD3',
     'B7H3',
     'PDL1',
     'CD8',
     'Stat3',
     'CD34',
     'MBP',
     'PDGFR',
     'GFAP',
     'SOX2',
     'CD163',
     'VDAC',
     'CD68',
     'Iba1',
     'VGFR2',
     'H3K27M',
     'HUD',
     'KI-67',
     'PD1']
####################################################################
def splitData_3(path_data,suffix):

    id = 272
    df_meta = pd.read_csv(os.path.join(path_data, 'preparedData', 'metadataCores_' + str(id) + suffix + '.csv'),
                          index_col=0)
    cores = df_meta.index.to_list()

    # get the number of tiles per core
    n_tiles = []
    i_failed = []
    for i, core in enumerate(cores):
        labelfile = 'labels_' + core + '_allMarkers.csv'
        try:
            df_core_label = pd.read_csv(os.path.join(path_data, 'preparedData', 'tiles', 'labels' + suffix, labelfile),
                                        index_col=0)
            n_tiles.append(df_core_label.shape[0])
        except:
            i_failed.append(i)
            n_tiles.append(np.nan)

    df_meta['n_tiles'] = n_tiles

    # Cores with no tiles
    cors_notiles = df_meta[df_meta['n_tiles'].isna()].index.to_list()
    df_meta.drop(cors_notiles, axis=0, inplace=True)

    df_meta.to_csv(os.path.join(path_data, 'preparedData', 'metadataCores_' + str(id) + suffix + '.csv'))

    # isDMG label
    df_label_core = pd.DataFrame(index=df_meta.index)
    df_label_core['isDMG'] = df_meta['H3K27M_posCells'] > 0
    df_label_core['isHealthy'] = df_meta.Diagnosis == 'CNTRL'
    df_label_core = df_label_core.astype('int')
    df_label_core['patientID'] = df_meta['PID']

    # Split 398 cores (ensure same patient in the relevant split),
    # Stratify by all labels
    col_y = 'is_DMG'
    n_splits = 5
    partition_tile, partition_core = getSplit(df_meta, col_y, n_splits)
    with open('partition_tile' + suffix, 'wb') as file:
        pickle.dump(partition_tile, file)
    with open('partition_core' + suffix, 'wb') as file:
        pickle.dump(partition_core, file)

    id = 273
    df_meta = pd.read_csv(os.path.join(path_data, 'preparedData', 'metadataCores_' + str(id) + suffix + '.csv'),
                          index_col=0)
    cores = df_meta.index.to_list()

    # get the number of tiles per core
    n_tiles = []
    i_failed = []
    for i, core in enumerate(cores):
        labelfile = 'labels_' + core + '_allMarkers.csv'
        try:
            df_core_label = pd.read_csv(os.path.join(path_data, 'preparedData', 'tiles', 'labels' + suffix, labelfile),
                                        index_col=0)
            n_tiles.append(df_core_label.shape[0])
        except:
            i_failed.append(i)
            n_tiles.append(np.nan)

    df_meta['n_tiles'] = n_tiles

    # Cores with no tiles
    cors_notiles = df_meta[df_meta['n_tiles'].isna()].index.to_list()
    df_meta.drop(cors_notiles, axis=0, inplace=True)

    df_meta.to_csv(os.path.join(path_data, 'preparedData', 'metadataCores_' + str(id) + suffix + '.csv'))

    # isDMG label
    df_label_core = pd.DataFrame(index=df_meta.index)
    df_label_core['isDMG'] = df_meta['H3K27M_posCells'] > 0
    df_label_core['isHealthy'] = df_meta.Diagnosis == 'CNTRL'
    df_label_core = df_label_core.astype('int')
    df_label_core['patientID'] = df_meta['PID']