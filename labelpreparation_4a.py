import os
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from sklearn.model_selection import StratifiedGroupKFold
import pickle
from tqdm import tqdm


def labelpreparation_4(suffix,path_data):
    id = 272
    id_test = 273
    path_labels = os.path.join(path_data,'preparedData')

    with open(os.path.join(path_labels, 'partition_core'+suffix), 'rb') as file:
        partition_core = pickle.load(file)

    cores = partition_core['train_0'][0]+partition_core['val_0'][0]
    df_labels = pd.DataFrame()

    for i, name in tqdm(enumerate(cores)):
        try:
            df_core = pd.read_csv(os.path.join(path_labels,'tiles','labels'+suffix,'labels_'+name+'_allMarkers.csv'), index_col = 0)
        except:
            print(name)
            continue

        #Format: 'tile_J-24_272_184'
        ind = 'tile_'+name+'_'+df_core.index.astype(str)
        df_core['ind'] = ind
        df_core.set_index('ind', inplace = True)
        df_core['core'] = name
        if i == 0:
            df_labels = df_core
        else:
            df_labels = pd.concat([df_labels,df_core], axis = 0)

    df_labels.to_csv(os.path.join(path_labels,'tiles','labels'+suffix+'.csv'))

    # Test set
    df_meta_test = pd.read_csv(os.path.join(path_data, 'preparedData', 'metadataCores_' + str(id_test) + suffix + '.csv'),
                               index_col=0)
    cores_test = df_meta_test.index.to_list()
    df_labels_test = pd.DataFrame()

    for i, name in enumerate(cores_test):

        try:
            df_core = pd.read_csv(
                os.path.join(path_labels, 'tiles', 'labels' + suffix, 'labels_' + name + '_allMarkers.csv'), index_col=0)
        except:
            print(name)
            continue

        # Format: 'tile_J-24_272_184'
        ind = 'tile_' + name + '_' + df_core.index.astype(str)
        df_core['ind'] = ind
        df_core.set_index('ind', inplace=True)
        df_core['core'] = name
        if i == 0:
            df_labels_test = df_core
        else:
            df_labels_test = pd.concat([df_labels_test, df_core], axis=0)

    df_labels_test.to_csv(os.path.join(path_labels, 'tiles', 'labels_TEST' + suffix + '.csv'))

    # get the number of tiles per core
    n_tiles = []
    i_failed = []
    for i, core in enumerate(cores_test):
        labelfile = 'labels_' + core + '_allMarkers.csv'
        try:
            df_core_label = pd.read_csv(os.path.join(path_data, 'preparedData', 'tiles', 'labels' + suffix, labelfile),
                                        index_col=0)
            n_tiles.append(df_core_label.shape[0])
        except:
            i_failed.append(i)
            n_tiles.append(np.nan)

    df_meta_test['n_tiles'] = n_tiles

    # Cores with no tiles
    cors_notiles = df_meta_test[df_meta_test['n_tiles'].isna()].index.to_list()
    df_meta_test.drop(cors_notiles, axis=0, inplace=True)

    df_meta_test.to_csv(os.path.join(path_data, 'preparedData', 'metadataCores_' + str(id_test) + suffix + '.csv'))


    # Training data
    id = 272
    df_meta = pd.read_csv(os.path.join(path_data, 'preparedData', 'metadataCores_' + str(id) + suffix + '.csv'),
                          index_col=0)
    cores = df_meta.index.to_list()

    # Get the number of tiles per core
    n_tiles = []
    i_failed = []
    for i, core in enumerate(cores):
        labelfile = 'labels_' + core + '_allMarkers.csv'
        try:
            df_core_label = pd.read_csv(os.path.join(path_data, 'preparedData', 'tiles', 'labels' + suffix, labelfile),
                                        index_col=0)
            n_tiles.append(df_core_label.shape[0])
        except:
            i_failed.append(i)
            n_tiles.append(np.nan)
    df_meta['n_tiles'] = n_tiles

    # Cores with no tiles
    cors_notiles = df_meta[df_meta['n_tiles'].isna()].index.to_list()
    df_meta.drop(cors_notiles, axis=0, inplace=True)
    df_meta.to_csv(os.path.join(path_data, 'preparedData', 'metadataCores_' + str(id) + suffix + '.csv'))