import pandas as pd
import os
from tqdm import tqdm
import matplotlib.patches as patches
import matplotlib.pyplot as plt
from matplotlib.colors import LinearSegmentedColormap

# Define custom colors
colors = [(0, 'blue'), (0.5, 'white'), (1, 'red')]
custom_cmap   = LinearSegmentedColormap.from_list("Custom", colors)
custom_min    = -1  # Minimum value
custom_max    = 1  # Maximum value
custom_center = 0  # Center value


def overlay_image_with_tiles(ax, image_path, dataframe, tile_size=50, opacity=0.2,
                             plot_true=False, title='a title'):
    # Load the image
    img = plt.imread(image_path)
    ax.imshow(img)

    # Iterate through DataFrame rows and draw tiles
    for index, row in dataframe.iterrows():
        # Extract coordinates and score from the DataFrame
        x = row['x']
        y = row['y']

        if plot_true == 'True':
            score = row['score_true']
        elif plot_true == 'Predicted':
            score = row['score_proba']
        elif plot_true == 'Diff':
            score = row['score_true'] - row['score_proba']

        # Create a rectangle patch
        if plot_true == 'Diff':
            # Normalize the data
            norm = plt.Normalize(vmin=custom_min, vmax=custom_max)
            rect = patches.Rectangle((x, y), tile_size, tile_size, linewidth=1, edgecolor='none',
                                     facecolor=custom_cmap(norm(score)), alpha=0.3)
        else:
            rect = patches.Rectangle((x, y), tile_size, tile_size, linewidth=1, edgecolor='none',
                                     facecolor=custom_cmap(score), alpha=0.3)

        # Add the patch to the Axes
        ax.add_patch(rect)

    ax.set_xticks([])
    ax.set_yticks([])


####################################################################
def visualization_7(path_data,suffix,cellmarker,resolution,side,min_cells,cores_in, core_image_path):
    shift_pix = int(0.5 * side / resolution)
    side_pix = int(side / resolution)
    clf_choice = 'lightGBM'
    output_model = os.path.join(path_data, 'models' + suffix)

    # Load metadata
    df_meta_272 = pd.read_csv(os.path.join(path_data, 'preparedData', 'metadataCores_' + str(272) + suffix + '.csv'),
                              index_col=0)
    df_meta_273 = pd.read_csv(os.path.join(path_data, 'preparedData', 'metadataCores_' + str(273) + suffix + '.csv'),
                              index_col=0)
    df_meta = pd.concat([df_meta_272, df_meta_273])

    # Load the predicted labels and probabilities
    for cv in range(0, 5):
        try:
            this_pred = pd.read_csv(os.path.join(output_model, 'tilesVHE_' + cellmarker + '_' + clf_choice + '_' + str(
                cv) + '_predictionProbabilities_TEST.csv'), index_col=0)
        except:
            print(cv)
            continue

        if cv == 0:
            df_pred_label_all = this_pred
        else:
            df_pred_label_all[str(cv)] = this_pred.loc[df_pred_label_all.index, :]

    # Combine all predictions
    df_pred_label = df_pred_label_all.mean(axis=1)

    cores = df_meta_273.index.to_list()
    cores_DMG = df_meta_273[
        df_meta_273.Diagnosis.isin(['DIPG', 'DMG spinal', 'DMG Thalamic', 'DMG Medulla', 'DMG cerebellum'])].index.to_list()
    cores_DMG_primary = df_meta_273[df_meta_273.Diagnosis.isin(
        ['DIPG', 'DMG spinal', 'DMG Thalamic', 'DMG Medulla', 'DMG cerebellum']) & df_meta_273.Dissemination.isin(
        ['Primary'])].index.to_list()
    cores_other_primary = df_meta_273[df_meta_273.Diagnosis.isin(['ATRT', 'HGG', 'LGG', 'Ependymoma', 'HGG Adult',
                                                                  'Medulloblastom']) & df_meta_273.Dissemination.isin(
        ['Primary'])].index.to_list()


    # Get predictions and true labels
    y_true = pd.DataFrame(index=df_pred_label_all.index.to_list(), columns=['True'])
    df_all_tiles = pd.DataFrame(columns=['cells', 'core'])
    for i_core, core in tqdm(enumerate(cores)):

        # Load core metadata
        df_core_cell = pd.read_csv(
            os.path.join(path_data, 'preparedData', 'tiles', 'labels' + suffix, 'labels_' + core + '_allMarkers.csv'),
            index_col=0)
        df_core_cell['ind'] = ['tile_' + core + '_' + str(i) for i in df_core_cell.index]
        df_core_cell.set_index('ind', inplace=True)

        # Labels
        tiles = df_core_cell.index.to_list()
        try:
            y_true.loc[tiles, 'True'] = (df_core_cell[cellmarker] > 0).to_list()
        except:
            continue
        df_all_tiles = pd.concat([df_all_tiles, df_core_cell[['cells', 'core']]], axis=0)


    # Visualize prediction, true and difference spatially
    for i_core, core in tqdm(enumerate(cores_in)):

        fig_pred, ax_pred = plt.subplots(1, 1, figsize=(2, 2))
        ax_pred.set_xticks([])
        ax_pred.set_yticks([])

        fig_true, ax_true = plt.subplots(1, 1, figsize=(2, 2))
        ax_true.set_xticks([])
        ax_true.set_yticks([])

        fig_diff, ax_diff = plt.subplots(1, 1, figsize=(2, 2))
        ax_diff.set_xticks([])
        ax_diff.set_yticks([])

        # Load core metadata
        df_core_cell = pd.read_csv(
            os.path.join(path_data, 'preparedData', 'tiles', 'labels' + suffix, 'labels_' + core + '_allMarkers.csv'),
            index_col=0)
        df_core_cell['ind'] = ['tile_' + core + '_' + str(i) for i in df_core_cell.index]
        df_core_cell.set_index('ind', inplace=True)

        # compare
        df_marker = pd.DataFrame()
        df_marker['predProba'] = df_pred_label.loc[df_core_cell.index].to_list()
        try:
            df_marker['True'] = (df_core_cell[cellmarker] > 0).to_list()
            df_marker['True'] = df_marker['True'].astype(int)
        except:
            print('Failed ' + core)
            continue

        # Get tile coordinates
        df_core = pd.read_csv(os.path.join(path_data, 'preparedData', 'cores' + suffix, 'cell_data_' + core + '.csv'),
                              index_col=0)
        min_x_core = df_core['x_loc'].min()
        min_y_core = df_core['y_loc'].min()
        max_x_core = df_core['x_loc'].max()
        max_y_core = df_core['y_loc'].max()

        # Get tiles
        min_x = min_x_core
        min_y = min_y_core
        counter = 0
        while min_y + side_pix < max_y_core:
            while min_x + side_pix < max_x_core:

                # Get coordinates
                max_x = min_x + side_pix
                max_y = min_y + side_pix

                # Check if cells are present
                df_tile = df_core.query('@min_x <= x_loc <= @max_x & @min_y <= y_loc <= @max_y')
                cell_tile = df_tile.index.to_list()
                if len(cell_tile) > min_cells:
                    # Get labels for this tile
                    df_marker.loc[counter, 'cells'] = len(cell_tile)
                    df_marker.loc[counter, 'core'] = core
                    df_marker.loc[counter, 'x_tile'] = min_x - min_x_core
                    df_marker.loc[counter, 'y_tile'] = min_y - min_y_core
                    df_marker.loc[counter, 'minx'] = min_x
                    df_marker.loc[counter, 'miny'] = min_y
                    df_marker.loc[counter, 'maxx'] = max_x
                    df_marker.loc[counter, 'maxy'] = max_y
                    counter += 1

                # Update column
                min_x = min_x + shift_pix

            # Update line and reset column
            min_y = min_y + shift_pix
            min_x = min_x_core

        x = df_marker['x_tile'].to_list()
        y = df_marker['y_tile'].to_list()
        score = (df_marker['predProba'] > 0.5).astype(int).to_list()
        score_proba = df_marker['predProba'].to_list()
        score_true = df_marker['True'].astype(int).to_list()

        data = {'x': x, 'y': y, 'score': score, 'score_true': score_true, 'score_proba': score_proba}
        df = pd.DataFrame(data)

        # Path to the image file (core images)
        image_path = os.path.join(core_image_path, core )

        # Overlay image with tiles based on the DataFrame
        title = core  + ' ' + df_meta.loc[core,'Dissemination']
        try:
            overlay_image_with_tiles(ax_true, image_path, df, tile_size=side_pix, plot_true='True', title=title)
            overlay_image_with_tiles(ax_pred, image_path, df, tile_size=side_pix, plot_true='Predicted', title=title)
            overlay_image_with_tiles(ax_diff, image_path, df, tile_size=side_pix, plot_true='Diff', title=title)
        except:
            continue

        fig_true.savefig(
            os.path.join(path_data, 'plots' + suffix, 'DMGcores', cellmarker + 'TEST_TrueTiles_DMG_' + core + '.png'),
            dpi=100)
        fig_pred.savefig(
            os.path.join(path_data, 'plots' + suffix, 'DMGcores', cellmarker + 'TEST_PredictedTiles_DMG_' + core + '.png'),
            dpi=100)
        fig_diff.savefig(os.path.join(path_data, 'plots' + suffix, 'DMGcores',
                                      cellmarker + 'TEST_DifferencesTiles_DMG_' + core + '.png'), dpi=100)

