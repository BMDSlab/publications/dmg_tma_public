import imageio
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import os
from pathlib import Path



####################################################################

# Get cell type fractions
dict_stainloc = {'CD68': 'Nucleus',
                 'CD3': 'Nucleus',
                 'CD8': 'Nucleus',
                 'KI-67': 'Nucleus',
                 'Iba1': 'Nucleus',
                 'H3K27M': 'Nucleus',
                 'CD163': 'Nucleus',
                 'CD31': 'Nucleus',
                 'CD34': 'Cell',
                 'GFAP': 'Cell',
                 'VGFR2': 'Cell',
                 'SOX2': 'Nucleus',
                 'HUD': 'Cell',
                 'B7H3': 'Nucleus',
                 'Nestin': 'Cell',
                 'Vimentin': 'Nucleus',
                 'PDGFR': 'Cell',
                 'PDL1': 'Cell',
                 'Stat3': 'Cell',
                 'PD1': 'Nucleus',
                 'MBP': 'Cytoplasm',
                 'VDAC': 'Cell',
                 }

# Note for IBA1 - use Cell mean if threshold is 2116
dict_markerspelling_1 = {
    'B7H3': 'B7H3',
    'CD3': 'CD3',
    'Iba1': 'IBA1',
    'H3K27M': 'H3K27M',
    'FOXG1': 'FOXG1',
    'KI-67': 'KI67',
    'CD8': 'CD8',
    'CD68': 'CD68',
    'CD163': 'CD163',
    'CD31': 'CD31',
    'CD34': 'CD34',
    'GFAP': 'GFAP',
    'VGFR2': 'VEGFR2',
    'SOX2': 'SOX2',
    'HUD': 'HUD',
    'Nestin': 'NESTIN',
    'Vimentin': 'VIMENTIN',
    'PDGFR': 'PDGFRA',
    'PDL1': 'PDL1',
    'Stat3': 'STAT3',
    'PD1': 'PD1',
    'MBP': 'MBP',
    'VDAC': 'VDAC'
}
dict_markerspelling = {v: k for k, v in dict_markerspelling_1.items()}


####################################################################
def preprocessData_2_allruns(suffix,path_data,filepath_exttest,slidename_exttest,
                             filepath_inttest, slidename_inttest,thresholdfile_train,
                             thresholdfile_test,shift_pix,side_pix,min_cells ):


    dotileImage = True


    output_png = os.path.join(path_data, 'preparedData', 'tiles', 'png' + suffix)
    output_npy = os.path.join(path_data, 'preparedData', 'tiles', 'npy' + suffix)
    output_csv = os.path.join(path_data, 'preparedData', 'tiles', 'labels' + suffix)

    # Run for all cores and TMAs
    for i_tma in [0,1]:
        for i_core in range(0, 500):

            useTest = i_tma

            # Names
            if useTest:
                id = 273
                filepath = filepath_exttest
                slide_name = slidename_exttest
                thresholdfile = thresholdfile_train
            else:
                id = 272
                thresholdfile = thresholdfile_test
                filepath = filepath_inttest
                slide_name = slidename_inttest

            # Create output
            Path(output_png).mkdir(parents=True, exist_ok=True)
            Path(output_npy).mkdir(parents=True, exist_ok=True)
            Path(output_csv).mkdir(parents=True, exist_ok=True)

            # Load data
            df_thresh = pd.read_csv(thresholdfile, index_col=0)
            df_meta   = pd.read_csv(os.path.join(path_data, 'preparedData', 'metadataCores_' + str(id) + suffix + '.csv'),
                                  index_col=0)
            cores = [m.split('_')[0] for m in df_meta.index.to_list()]

            # Load image
            if dotileImage:
                input_tif_path = os.path.join(path_data, 'data', slide_name)
                img = imageio.imread(input_tif_path)
                tif_data = imageio.imread(input_tif_path)

            # Loop over cores
            try:
                for core in [cores[i_core]]:

                    name = core + '_' + str(id)
                    print('working on ' + name)

                    # Get core subset
                    df_core = pd.read_csv(os.path.join(path_data, 'preparedData', 'cores' + suffix, 'cell_data_' + name + '.csv'),
                                          index_col=0)
                    markers = list(set(df_core.columns) - set(['x_loc', 'y_loc', 'Centroid X Âµm', 'Centroid Y Âµm']))
                    min_x_core = df_core['x_loc'].min()
                    min_y_core = df_core['y_loc'].min()
                    max_x_core = df_core['x_loc'].max()
                    max_y_core = df_core['y_loc'].max()

                    if dotileImage:
                        # Get core image
                        cropped_img = tif_data[min_y_core:max_y_core, min_x_core:max_x_core, :]

                    # Get tiles
                    min_x = min_x_core
                    min_y = min_y_core
                    df_data = pd.DataFrame()
                    counter = 0
                    while min_y + side_pix < max_y_core:
                        while min_x + side_pix < max_x_core:

                            # Get coordinates
                            max_x = min_x + side_pix
                            max_y = min_y + side_pix

                            # Check if cells are present
                            df_tile = df_core.query('@min_x <= x_loc <= @max_x & @min_y <= y_loc <= @max_y')
                            cell_tile = df_tile.index.to_list()
                            if len(cell_tile) > min_cells:

                                # Get labels for this tile
                                df_data.loc[counter, 'cells'] = len(cell_tile)
                                df_data.loc[counter, 'core'] = name
                                df_data.loc[counter, 'minx'] = min_x
                                df_data.loc[counter, 'miny'] = min_y
                                df_data.loc[counter, 'maxx'] = max_x
                                df_data.loc[counter, 'maxy'] = max_y

                                # Get markers and cell numbers
                                for col in markers:
                                    m = col.split(': ')[1].split(' mean')[0]

                                    try:
                                        thresh = float(df_thresh.loc[core, dict_markerspelling[m]])
                                    except:
                                        print('failed ' + m + ' for core ' + core)
                                        continue

                                    if m == 'IBA1':
                                        if thresh == 2116:
                                            col = 'Cell' + ': ' + m + ' mean'
                                    df_data.loc[counter, m] = df_tile[df_tile[col] > thresh].shape[0]

                                if dotileImage:
                                    tilename = 'tile_' + name + '_' + str(counter)
                                    print(tilename)
                                    im_crop = tif_data[min_y:max_y, min_x:max_x, :]
                                    plt.imsave(os.path.join(output_png, tilename + '.png'), im_crop, format='png')
                                    np.save(os.path.join(output_npy, tilename + '.npy'), im_crop)

                                counter += 1

                            # Update column
                            min_x = min_x + shift_pix

                        # Update line and reset column
                        min_y = min_y + shift_pix
                        min_x = min_x_core
                    df_data.to_csv(os.path.join(output_csv, 'labels_' + name + '_allMarkers.csv'))
            except:
                print('Core failed - check if TMA contains this core!')